// http method
// get- retrieve resources
// Post - send a data to creating a resources
// put - sends data for updating resources
// Delete - Delete a specified resource



// Postman Client - an API or platform to perform the method to get the respond

let http = require("http");

http.createServer(function(request, response){
	if(request.url == "/items" && request.method == "GET"){
		response.writeHead(200, {'Content-Type': 'text/Plain'});
		response.end('Data retrieved from data base');
	}
}).listen(4000);

console.log('Server is running at localhost : 4000')